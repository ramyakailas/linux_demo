#
# Cookbook Name:: linux_demo
# Recipe:: default
#
# Copyright (c) 2016 The Authors, All Rights Reserved.

package 'httpd' do
  action :install
end

template '/var/www/html/index.html' do
  source 'index.html.erb'
  mode 00644
  notifies :restart, 'service[httpd]', :immediately
  variables(message: node['linux_demo']['message'])
end

service 'httpd' do
  supports status: true
  action [:enable, :start]
end

package vim

package tree